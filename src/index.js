import React from 'react'
import ReactDOM from 'react-dom'
import SeasonDisplay from './SeasonDisplay'
import { changeExt } from 'upath';
import Loader from './loader'

class App extends React.Component{
  state = {lat: null, errMsg: ''}

  componentDidMount(){
    window.navigator.geolocation.getCurrentPosition(
      (position)=> {
        let latti = position.coords.latitude
        this.change(latti)
      },
      (err)=> {
        this.setState({
          errMsg : err.message
        })
        
      }
      
    )
  }
  change(latti){
    this.setState(
      {
        lat: latti
      }
    )
  }

  renderContent(){
    if(this.state.errMsg && !this.state.lat){
      return(
        <div>
          Error : {this.state.errMsg}
        </div>
      );
    }

    if(this.state.lat && !this.state.errMsg){
      
      return(
        <div>
          <SeasonDisplay/>
        </div>

      );
    }

    
      return(
        <Loader text=" Plwase Allow the Location"/>
      );
  }
  render(){
    
    return(
      <div className="border-red">
      {this.renderContent()}
      </div>
    );
    
    
  }
}


ReactDOM.render(<App/>, document.getElementById('root'))