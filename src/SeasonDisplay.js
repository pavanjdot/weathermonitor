import './SeasonDisplay.css';
import React from 'react';

const seasonConfig = {

  summer : {
    text: "Let's hit the beach!",
    iconName: "sun"
  },
  winter : {
    text: "Brrh! its Chilled",
    iconName: "snowflake"
  }

}

const DetermineSeason = (latti, month)=>{
  if(month >2 && month < 9){
    return latti > 0 ? "summer": "winter"
  }
  else{
    return latti > 0 ? "winter" : "summer"
  }
};

const SeasonDisplay = (props) =>{

  const season = DetermineSeason(props.lattitude, Date.month);
  const {text, iconName } = seasonConfig[season]
  return(
    <div className={`season-display ${season}`}>
      <i className={`iconleft massive ${iconName} icon`}/>
      <h1>{text}</h1>
      <i className={`iconright massive ${iconName} icon`}/>
    </div>
  );
}

export default SeasonDisplay;